#########################################################################
## Handler for Muon tests in Gauss, takes a root file containing       ##
## muon test data and saves as JSON. Muon test data will be            ##
## located in the MuonTestResults folder after running runmuontest.py  ##
## MUST be run in the same directory as runmuontest.py is              ##
## @Author: R.Calladine                                                ##
## @date:   Last modified 2016-12-12                                   ##
#########################################################################

import json, os
from ROOT import TFile, TH1D, TTree, gDirectory

from BaseHandler import BaseHandler


# Defines a function to get data from the histograms once created
def histogram_to_array(hist):
    retlist = []
    for i in range(int(hist.GetEntries())):
        retlist.append([hist.GetBinCenter(i), hist.GetBinContent(i), hist.GetBinError(i)])
    return retlist


class MuonMoniTestHandler(BaseHandler):

    def __init__(self):
        super(self.__class__, self).__init__()

    def collectResults(self, directory):
       inroot = "MuonTestResults/MuonMoniSim_histos.root"
       inpath = os.path.join(directory,inroot) 
       infile = TFile.Open(inpath) 

    #####  Collects results from MuonHitChecker  #####
   
    #  Histograms in the input file are numbered 1000-1019 for time multiplicities
    #                                            2000-2019 for radial multiplicites
    #  Below gets the histograms and stores them in lists

       histID_TM = [gDirectory.Get('MuonHitChecker/1{0:03}'.format(i)) for i in range(0, 20)]
       histID_RM = [gDirectory.Get('MuonHitChecker/2{0:03}'.format(i)) for i in range(0, 20)]
      
       Mu_Station = 0
    # Creates a dictionary to hold the histograms, key being their titles and their values being 
    # the data from the corresponding histograms
       MuHitCheck_dict = {}
           
    # Adds each of the 4 histograms for each of the muon stations together
       for n in range(0,20):
           if n % 4 == 0:
               Mu_Station += 1
               TM_hist = histID_TM[n].Clone("Time_multiplicity_M{mu}".format(mu=Mu_Station))
               RM_hist = histID_RM[n].Clone("Radial_multiplicity_M{mu}".format(mu=Mu_Station))
           else:
               TM_hist.Add(histID_TM[n],1)
               RM_hist.Add(histID_RM[n],1)
    # Ensures every four histograms are added together, remembering we start from 1000
           if n != 0 and n % 4 == 3:
               MuHitCheck_dict[TM_hist.GetName()] = [histogram_to_array(TM_hist)]
               MuHitCheck_dict[RM_hist.GetName()] = [histogram_to_array(RM_hist)]
                           
    # Saves histograms as JSON
       for key in MuHitCheck_dict:
           self.saveJSON(key, MuHitCheck_dict[key], key)

    ##### Collects results from MuonMultipleScatteringChecker #####
       
       hist_MuSct = []
       
       for i in range(1,5):
           hist_MuSct.append(gDirectory.Get('MuonMultipleScatteringChecker/MuonMultipleScatteringTest/dxdTx_MF{}'.format(i)))
           hist_MuSct.append(gDirectory.Get('MuonMultipleScatteringChecker/MuonMultipleScatteringTest/dydTy_MF{}'.format(i)))
           hist_MuSct.append(gDirectory.Get('MuonMultipleScatteringChecker/MuonMultipleScatteringTest/dxp_MF{}'.format(i)))
           hist_MuSct.append(gDirectory.Get('MuonMultipleScatteringChecker/MuonMultipleScatteringTest/dyp_MF{}'.format(i)))
           hist_MuSct.append(gDirectory.Get('MuonMultipleScatteringChecker/MuonMultipleScatteringTest/dTxp_MF{}'.format(i)))
           hist_MuSct.append(gDirectory.Get('MuonMultipleScatteringChecker/MuonMultipleScatteringTest/dTyp_MF{}'.format(i)))

    
       MuSct_dict = {j.GetName():j for j in hist_MuSct}
       
    # Saves histograms as JSON
       for key in MuSct_dict:
           self.saveJSON(key, MuSct_dict[key], key)
