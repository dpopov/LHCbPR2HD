import os, sys, re
from BaseHandler import BaseHandler
import random

class DummyHandler(BaseHandler):
        
    def __init__(self):
        super(self.__class__, self).__init__()
        self.finished = False
        self.results = []

    def collectResults(self,directory):
        self.saveFloat("MyMeasure", random.gauss(42, 5), "Dummy timing value [ms]", "Timing")



