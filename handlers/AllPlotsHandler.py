import os

import ROOT

from BaseHandler import BaseHandler

def _TList__iter__(self):
    next_item = ROOT.TIter(self)
    while True:
        curr_item = next_item()
        if curr_item:
            yield curr_item
        else:
            raise StopIteration

ROOT.TList.__iter__ = _TList__iter__

def make_key(prefix, obj):
    the_type = type(obj)
    return "{}_{}_{}".format(
        prefix,
        obj.ClassName(),
        obj.GetName(),
    )

def all_plots_in_files(filenames):
    def loop_over(key, tdirectory):
        tlist_of_keys = tdirectory.GetListOfKeys()
        for obj_key in tlist_of_keys:
            obj = tdirectory.Get(obj_key.GetName())
            if obj.InheritsFrom("TH1") or obj.InheritsFrom("TGraph"):
                yield make_key(key, obj), obj
            elif obj.InheritsFrom("TDirectory"):
                for key_recur, obj_recur in loop_over(make_key(key, obj), obj):
                    yield key_recur, obj_recur
            else:
                assert False, "Shouldn't get here."

    for fname in filenames:
        try:
            f = ROOT.TFile.Open(fname)
            for key, obj in loop_over(make_key("", f), f):
                yield key, obj
        finally:
            if f:
                f.Close()


class AllPlotsHandler(BaseHandler):
    """A handler that takes all plots from given files and puts them into JSON.

    All it requires to work is the class attribute 'files_to_search' e.g.

    class MySimpleHandler(AllPlotsHandler):
        files_to_search = ['foobar.root']

    The class will then search recursively through the ROOT file for anything
    that inherits from TH1 or TGraph and save it to the database with
    self.saveJSON. The plots are saved with a key based on the location of the
    plot in the file e.g. for a histogram "h2" in a file "/tmp/tmpUbcpvn.root"
    in the TDirectory "newdir" becomes:

        _TFile_/tmp/tmpUbcpvn.root_TDirectoryFile_newdir_TH1D_h2

    For more information, examine the json_results produced by this handler and
    the make_key function.
    """

    def collectResults(self, directory):
        filenames = [os.path.join(directory, i) for i in type(self).files_to_search]

        allplots_keys = []
        for key, obj in all_plots_in_files(filenames):
            self.saveJSON(
                key,
                obj,
                group="_allplots"
                )
            allplots_keys.append(key)

        self.saveJSON(
            "allplots_keys",
            allplots_keys,
            description="A list of all names of JSON'd plottables recorded by AllPlotsHandler.",
            group="_allplots_keys",
        )
